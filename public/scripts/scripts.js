var hasTopShare = false;
var hasTopFilter = false;

if ($(".shareIcon")[0]) {
    hasTopShare = true;
}

if ($(".filteroptions")[0]) {
    hasTopFilter = true;
}
// Top Share
if (hasTopShare === true) {
    $(".shareIcon").click(function(){
        if($('.dropdownContainer').is(':visible')) {
            $(".dropdownContainer").fadeToggle(300, "linear");
        }
        $(".share .dropdown").fadeToggle(300, "linear");
    });
}

//Top Filter
if (hasTopFilter === true) {
    $(".filteroptions").click(function(){
        if($('.share .dropdown').is(':visible')) {
            $(".share .dropdown").fadeToggle(300, "linear");
        }
        if($(this).parent().parent().parent().attr('id') === 'featuredVideo') {
            $('#moreprojectlist.dropdown-inline .dropdownContainer').css('display', "none !important");
        }
        else if($(this).parent().parent().parent().attr('id') === 'moreprojectlist') {
            $('#featuredVideo.dropdown-inline .dropdownContainer').css('display', "none !important");
        }
        $(".dropdownContainer").fadeToggle(300, "linear");
    });
}


// $(selector).toggle(speed,easing,callback);
//menu animation-I stole this from some guy named Nick
(function () {

    "use strict";

    var toggles = document.querySelectorAll(".mainmenu");    
    $('.secondlinksblurb').hide();
    $('.mobileblurbUp').hide();

    $('.blurb').mouseenter(function () {        
        if ($(window).width() <= 768) {
            $('.secondlinksblurb').hide();
        }
        else
            $('.secondlinksblurb').show();
    });
    $('.blurb').mouseleave(function () {
        if ($(window).width() > 768) {
            $('.secondlinksblurb').hide();
        }

    });
    $('.mobileblurb').click(function (e) {        
        if ($('.mobileblurbUp').css('display') == 'none') {
            $('.secondlinksblurb').show();
            $('.mobileblurbUp').show();
            $('.mobileblurbDown').hide();
        }
        else {
            $('.secondlinksblurb').hide();
            $('.mobileblurbUp').hide();
            $('.mobileblurbDown').show();
        }
        return false;
    });

    for (var i = toggles.length - 1; i >= 0; i--) {
        var toggle = toggles[i];
        toggleHandler(toggle);
    };

    function toggleHandler(toggle) {
        toggle.addEventListener("click", function (e) {
            e.preventDefault();
            (this.classList.contains("active") === true) ? this.classList.remove("active"): this.classList.add("active");
        });
    }

})();


$(function () {
    var didScroll = false,
        lastScrollTop = 0,
        delta = 5,
        contentStart = 0,
        navbarHeight = $('header').outerHeight();

    $(window).scroll(function (e) {
        if ($(window).scrollTop() >= contentStart) {
            didScroll = true;
        } else {
            didScroll = false;
        }
    });
    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 500);

    function hasScrolled() {
     
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta) return;

        if (st > lastScrollTop && st > navbarHeight && !$('.mainmenu').hasClass('active')) {
            $('header').addClass('nav-up');
            $('.floatme').addClass('nav-up');

        } else if (st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up');
            $('.floatme').removeClass('nav-up');
        }

        lastScrollTop = st;
    }
});


$(window).load(function () {
    if ($(window).scrollTop() > 100) {
        $('header').addClass('fixedscroll');

    } else {
        $('header').removeClass('fixedscroll');

    }
});


$(window).scroll(function () {
    
    var moreprojectsPosition = 0;
    if($( window ).width() <= 768) {
        if ($('.dropdownContainer').is(':visible')) {
            $(".dropdownContainer").fadeOut(300, "linear");
        }
          
        if ($('.dropdownContainer.ourTeamDropdown').is(':visible')) {
            $(".dropdownContainer.ourTeamDropdown").fadeOut(300, "linear");
        }
    }
    else {
        $('#moreprojects.dropdown-inline ul li:nth-child(1) a').removeClass('active');
        $('.ourTeam .dropdownContainer').css( "visibility", "visible" );
        $('.ourTeam .dropdownContainer').css( "display", "block" );
          
    }
    if ($('.share .dropdown').is(':visible')) {
        $('.share .dropdown').fadeOut(300, "linear");
    }
     
    if ($("#moreprojects").length > 0) {
        moreprojectsPosition = (jQuery("#moreprojects").position().top - 100);
    }
     
    if ($(window).scrollTop() > moreprojectsPosition && !$('.overlay').hasClass('shown')) {
        $('.filterclasstop').addClass('filtershow');
    } else {
        $('.filterclasstop').removeClass('filtershow');
    }
     
    //Add more logic for menu click, when menu click
    if ($('.overlay').hasClass('shown')) {
        $('.filterclass').addClass('filterhide');
    }
    else {
        $('.filterclass').removeClass('filterhide');
    }
    if ($('#stpulldown').is(':visible')) {
        $('#stpulldown').hide();
    }
});


$(window).scroll(function () {
    if ($(window).scrollTop() > 100 && !$('.overlay').hasClass('shown')) {
        $('header').addClass('fixedscroll');

    } else {
        $('header').removeClass('fixedscroll');

    }
});


$('.mainmenu').click(function () {
    if (hasTopShare === true) {
        if($('.share .dropdown').is(':visible')) {
            $(".share .dropdown").fadeToggle(300, "linear");
        }
    }
        
    if (hasTopFilter === true) {
        if($('.dropdownContainer.filter').is(':visible')) {
            $(".dropdownContainer.filter").fadeToggle(300, "linear");
        }
    }
    if ($(window).scrollTop() < 100 && !$('.overlay').hasClass('shown')) {
        $('.dropdownContainer').css('display', 'none');
        $('.overlay').addClass('shown');
        $('body').addClass('blerp');
        $('.filterclass').addClass('filterhide');
        $('header').removeClass('fixedscroll');
        // $('.logo').addClass('white');
        // $('.logo').removeClass('colored');
        if (hasTopShare === true) { $('.shareIcon').hide(); }
        if (hasTopShare === true) { $('.share').hide(); }
        if (hasTopFilter === true) { $('.filterclasstop.topFilter').hide(); }
    } else if ($(window).scrollTop() > 100 && !$('.overlay').hasClass('shown')){
        $('.dropdownContainer').css('display', 'none');
        $('.overlay').addClass('shown');
        $('body').addClass('blerp');
        $('.filterclass').addClass('filterhide');
        $('header').removeClass('fixedscroll');
        // $('.logo').addClass('white');
        // $('.logo').removeClass('colored');
        if (hasTopShare === true) { $('.shareIcon').hide(); }
        if (hasTopShare === true) { $('.share').hide(); }
        if (hasTopFilter === true) { $('.filterclasstop.topFilter').hide(); }
    } else if ($(window).scrollTop() > 100 && $('.overlay').hasClass('shown')){
        $('.dropdownContainer').css('display', 'block');
        $('header').addClass('fixedscroll');
        $('.overlay').removeClass('shown');
        $('body').removeClass('blerp');
        $('.filterclass').removeClass('filterhide');
        // $('.logo').removeClass('white');
        // $('.logo').addClass('colored');
        if (hasTopShare === true) { $('.shareIcon').show(); }
        if (hasTopShare === true) { $('.share').show(); }
        if (hasTopFilter === true) { $('.filterclasstop.topFilter').show(); }
    } else {
        $('.dropdownContainer').css('display', 'block');
        $('.overlay').removeClass('shown');
        $('body').removeClass('blerp');
        $('.filterclass').removeClass('filterhide');
        // $('.logo').removeClass('white');
        // $('.logo').addClass('colored');
        if (hasTopShare === true) { $('.shareIcon').show(); }
        if (hasTopShare === true) { $('.share').show(); }
        if (hasTopFilter === true) { $('.filterclasstop.topFilter').show(); }
    }
     

    if ($('.overlay').hasClass('shown')) {
        $('.filterclasstop').addClass('filterhide');
    }
    else {
        $('.filterclasstop').removeClass('filterhide');
    }
    return false;
});
//var getLocation = function (href) {
//    var l = document.createElement("a");
//    l.href = href;
//    return l;
//};
$('.mainlinks, .secondlinks').on('click', function() {    
    //$(this).children('a').attr('href')
    //var l = getLocation($(this).children('a').attr('href'));
    //if (window.location.hostname != l.hostname)
    //{        
    //    var r = confirm("This link will redirect to another site. Do you want to continue!");
    //    if (r == true) {            
    //        window.open($(this).children('a').attr('href'),'_blank');
    //    } 
    //    return false;
    //}
    $('.dropdownContainer').css('display', 'block');
    $('.overlay').removeClass('shown');
    $('body').removeClass('blerp');
    $('.filterclass').removeClass('filterhide');
    $('.logo').removeClass('white');
    $('.logo').addClass('colored');
    $('.mainmenu').removeClass('active');
});

$(window).scroll(function (event) {
    // $('.dropdownContainer').hide();
});

// $(window).click(function () {
//   if($( window ).width() <= 768) {
//      $('.dropdownContainer').hide();
//      if (dropdownVisible3 === false) {
//           $('.dropdownContainer.ourTeamDropdown').css('display', 'block');
//           $('.dropdownContainer.ourTeamDropdown').animate({ opacity: 1 }, 500);
//           dropdownVisible3 = true;
//     }
//     else {
//           $('.dropdownContainer.ourTeamDropdown').css('display', 'none');
//           $('.dropdownContainer.ourTeamDropdown').animate({ opacity: 0 }, 500);
//           dropdownVisible3 = false;
//     }
//    }
// });

var dropdownVisible = false;
var dropdownVisible2 = false;
var dropdownVisible3 = false;

$('.filteroptions.ourTeam').click(function (event) {
    event.stopPropagation();
    //$('.dropdown-inline ul li:nth-child(1) a').removeClass('active');
    if($( window ).width() <= 768) {
        if (dropdownVisible3 === false) {
            $('.dropdownContainer.ourTeamDropdown').animate({ opacity: 1 }, 500);
            dropdownVisible3 = true;
        }
        else {
            $('.dropdownContainer.ourTeamDropdown').animate({ opacity: 0 }, 500);
            dropdownVisible3 = false;
        }
    }
});

$('#moreprojectlist .filteroptions').click(function (event) {
    event.stopPropagation();
   // $('#moreprojectlist.dropdown-inline ul li:nth-child(1) a').removeClass('active');
    if($( window ).width() <= 768) {
        $("#featuredVideo.dropdown-inline .dropdownContainer").css('display', 'none');
        if (dropdownVisible === false) {
            $("#moreprojectlist .dropdownContainer.filterOn").animate({ opacity: 1 }, 500);
            dropdownVisible = true;
        }
        else {
            $("#moreprojectlist .dropdownContainer.filterOn").animate({ opacity: 0 }, 500);
            dropdownVisible = false;
        }
    }
    // alert('2');
});
         
$('#featuredVideo .filteroptions').click(function (event) {
    event.stopPropagation();    
    //$('featuredVideo.dropdown-inline ul li:nth-child(1) a').removeClass('active');
    if($( window ).width() <= 768) {
        $("#moreprojectlist.dropdown-inline .dropdownContainer").css('display', 'none');
        if (dropdownVisible2 === false) {
            $("#featuredVideo .dropdownContainer.filterOn").animate({ opacity: 1 }, 500);
            dropdownVisible2 = true;
        }
        else {
            $("#featuredVideo .dropdownContainer.filterOn").animate({ opacity: 0 }, 500);
            dropdownVisible2 = false;
        }
    }
    // alert('3');
});

function getDateInYears(years) {
    var date = new Date();
    return new Date(date.setFullYear(date.getFullYear() + years));
}

var $container = $('.announcement-panel');
function setbactToTopHeight() {
    var $heightBt = $('.backToTop').outerHeight(),
        $heightAb = ($container.is(':visible')) ? $container.outerHeight() : 0,
        $totalHeightBt = $heightBt + $heightAb;

    $('.backToTop').css('margin-top', -$totalHeightBt);
}

var isCookiePolicyAccepted = !!($.cookie("is-cookie-policy-accepted"));
if (!isCookiePolicyAccepted) {
	$('html').addClass('announcement-banner-show');
    $container.show();
    setbactToTopHeight();
}
else {
	$('html').removeClass('announcement-banner-show');
    $container.hide();
    setbactToTopHeight();
}
if (!isCookiePolicyAccepted) {
    $('.announcement-close').on("click", function () {
        var date = getDateInYears(2);
        $.cookie("is-cookie-policy-accepted", true, { path: "/", expires: date });
        $container.slideUp(600);
        $(this).parents('div').fadeOut();
		$('html').removeClass('announcement-banner-show');
        $('.backToTop').css('margin-top', -($('.backToTop').outerHeight()));
    });
}
$(window).on("orientationchange load resize", function () {
    setbactToTopHeight();
});
$('.reset-button').click(function myfunction() {
    $('.portfolio-sorting li a').removeClass('active');
    $('.dropdown-heading *').removeClass('active');
    $('.portfolio-sorting').css('display', "none");
    $('.imgDown').css('display', "inline-block");
    $('.imgUp').css('display', "none");
    if ($(window).width() <= 767) {
        $('.portfolio-sorting').slideUp();        
        $('.imgDown').css('display', "none");
    }
});